package com.ben.microservice.microservicemybatis.mapper;

import com.ben.microservice.microservicemybatis.dto.BlogEntity;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface BlogMapper {

    @Select("select * from blog")
    List<BlogEntity> getAllUsers();
}
