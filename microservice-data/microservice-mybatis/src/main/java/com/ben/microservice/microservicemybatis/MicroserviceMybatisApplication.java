package com.ben.microservice.microservicemybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(basePackages = "com.ben.microservice.microservicemybatis.mapper")
public class MicroserviceMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceMybatisApplication.class, args);
    }

}
