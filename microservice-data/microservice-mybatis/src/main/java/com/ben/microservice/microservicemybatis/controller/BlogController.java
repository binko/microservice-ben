package com.ben.microservice.microservicemybatis.controller;

import com.ben.microservice.microservicemybatis.dto.BlogEntity;
import com.ben.microservice.microservicemybatis.mapper.BlogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BlogController {

    @Autowired
    BlogMapper blogMapper;

    @RequestMapping("/query")
    public List<BlogEntity> queryUser() {
        List<BlogEntity> list = blogMapper.getAllUsers();
        return list;
    }
}
