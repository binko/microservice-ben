package com.ben.microservice.microservicephoenix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroservicePhoenixApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroservicePhoenixApplication.class, args);
    }

}
