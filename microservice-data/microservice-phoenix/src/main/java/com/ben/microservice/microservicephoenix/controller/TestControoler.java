package com.ben.microservice.microservicephoenix.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestControoler {

    @Autowired
    @Qualifier("phoenixJdbcTemplate")
    JdbcTemplate phoenixJdbcTemplate;

    @GetMapping("/test")
    public String getTest(){

        phoenixJdbcTemplate.execute("CREATE TABLE ben_test ( id BIGINT not null primary key, date Date)");
        return "123";
    }

}
