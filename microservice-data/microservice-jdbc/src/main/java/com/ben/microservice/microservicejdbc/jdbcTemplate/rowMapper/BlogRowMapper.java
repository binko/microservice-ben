package com.ben.microservice.microservicejdbc.jdbcTemplate.rowMapper;

import com.ben.microservice.microservicejdbc.dbutils.dto.BlogDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BlogRowMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        BlogDto blog = new BlogDto();
        blog.setBid(resultSet.getInt("bid"));
        blog.setName(resultSet.getString("name"));
        blog.setAuthorId(resultSet.getInt("author_id"));
        return blog;
    }
}