package com.ben.microservice.microservicejdbc.jdbcTemplate;

import com.ben.microservice.microservicejdbc.dbutils.dto.BlogDto;
import com.ben.microservice.microservicejdbc.jdbcTemplate.rowMapper.BaseRowMapper;
import com.ben.microservice.microservicejdbc.jdbcTemplate.rowMapper.BlogRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JdbcServiceImpl implements JdbcService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<BlogDto> selectList() {
        String sql = "select * from blog";
        //List<BlogDto> blogs = jdbcTemplate.query(sql,new BlogRowMapper());
        //List<BlogDto> blogs = jdbcTemplate.query(sql,new BaseRowMapper<>(BlogDto.class)); // 通用mapper
        List<BlogDto> blogs = jdbcTemplate.query(sql,new BeanPropertyRowMapper<BlogDto>(BlogDto.class));//自带通用

        return blogs;
    }

}
