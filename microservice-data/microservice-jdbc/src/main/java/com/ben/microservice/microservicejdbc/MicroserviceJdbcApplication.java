package com.ben.microservice.microservicejdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceJdbcApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceJdbcApplication.class, args);
    }

}
