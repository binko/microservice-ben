package com.ben.microservice.microservicejdbc.jdbcTemplate;

import com.ben.microservice.microservicejdbc.dbutils.dto.BlogDto;

import java.sql.SQLException;
import java.util.List;

public interface JdbcService {


    List<BlogDto> selectList() throws Exception;
}
