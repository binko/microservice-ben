package com.ben.microservice.microservicejdbc.dbutils;

import com.ben.microservice.microservicejdbc.dbutils.dao.BlogDao;

/**
 * 不依赖spring，直接获取datasource
 */
public class QueryRunnerTest {

    public static void main(String[] args) throws Exception {
        HikariUtil.init();
        System.out.println(BlogDao.selectBlog(1));
        // Language Level 设置成 Java 8
        BlogDao.selectList();
    }
}
