package com.ben.microservice.microservicejdbc.jdbcTemplate;

import com.alibaba.fastjson.JSONArray;
import com.ben.microservice.microservicejdbc.dbutils.dto.BlogDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class JDBCTemplateController {

    @Autowired
    private JdbcService jdbcService;

    @GetMapping("/getBlogs")
    public String getBlogs() throws Exception{
        List<BlogDto> blogDtos = jdbcService.selectList();
        return JSONArray.toJSON(blogDtos).toString();
    }
}
