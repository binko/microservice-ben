package com.ben.microservice.microservicejdbc.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * 原生的 jdbc 语句
 */
public class JDBCTest {
    //首先，pom.xml 引入mysql驱动依赖
    public static void main(String[] args) throws Exception{
        //1、注册JDBC驱动
        Class.forName("com.mysql.jdbc.Driver");
        //2、打开连接
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/jdbc_test","root","root");
        //3、执行查询
        Statement statement = conn.createStatement();
        ResultSet rs = statement.executeQuery("select * from blog");
        //4、获取结果集
        while (rs.next()){
            int id = rs.getInt("bid");
            String name = rs.getString("name");
            System.out.println("id:="+id+",name="+name);
        }
        rs.close();
        statement.close();
        conn.close();
    }
}
