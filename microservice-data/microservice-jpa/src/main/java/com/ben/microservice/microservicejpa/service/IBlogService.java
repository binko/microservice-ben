package com.ben.microservice.microservicejpa.service;


import com.ben.microservice.microservicejpa.domain.Blog;

import java.util.List;

public interface IBlogService {
    public List<Blog> queryBlog();

    public void addBlog(Blog blog);
}
