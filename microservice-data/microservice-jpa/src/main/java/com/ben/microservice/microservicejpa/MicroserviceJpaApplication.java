package com.ben.microservice.microservicejpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroserviceJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceJpaApplication.class, args);
    }

}
