package com.ben.microservice.microservicejpa.dao;

import com.ben.microservice.microservicejpa.domain.Blog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogDao extends JpaRepository<Blog, Integer> {

}