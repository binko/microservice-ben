package com.ben.microservice.microservicejpa.service;

import com.ben.microservice.microservicejpa.dao.BlogDao;
import com.ben.microservice.microservicejpa.domain.Blog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogServiceImpl implements IBlogService {
    @Autowired
    private BlogDao blogDao;

    @Override
    public List<Blog> queryBlog() {
        return blogDao.findAll();
    }

    @Override
    public void addBlog(Blog blog)
    {
        blogDao.save(blog);
    }
}
