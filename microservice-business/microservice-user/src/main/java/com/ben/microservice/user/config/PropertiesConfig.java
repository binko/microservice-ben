package com.ben.microservice.user.config;

import com.ben.microservice.user.core.beetl.BeetlConfiguration;
import com.ben.microservice.user.core.properties.BeetlProperties;
import com.ben.microservice.user.core.properties.GunsProperties;
import org.beetl.core.resource.ClasspathResourceLoader;
import org.beetl.ext.spring.BeetlSpringViewResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.security.auth.login.LoginContext;

@Configuration
public class PropertiesConfig {

    /**
     * beetl模板的配置
     *
     * @author fengshuonan
     * @Date 2019-06-13 08:55
     */
    @Bean
    @ConfigurationProperties(prefix = BeetlProperties.BEETLCONF_PREFIX)
    public BeetlProperties beetlProperties() {
        return new BeetlProperties();
    }

    /**
     * Guns的属性配置
     *
     * @author fengshuonan
     * @Date 2019-06-13 08:56
     */
    @Bean
    @ConfigurationProperties(prefix = GunsProperties.PREFIX)
    public GunsProperties gunsProperties() {
        return new GunsProperties();
    }

}
