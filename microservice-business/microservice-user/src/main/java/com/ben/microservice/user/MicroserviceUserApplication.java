package com.ben.microservice.user;

import cn.stylefeng.roses.core.config.MybatisDataSourceAutoConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.mybatis.spring.annotation.MapperScans;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@Slf4j
@ComponentScan("com.ben.microservice")
@MapperScan("com.ben.microservice.user.**.mapper")
@SpringBootApplication(exclude = {MybatisDataSourceAutoConfiguration.class})
public class MicroserviceUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceUserApplication.class, args);
        log.info(MicroserviceUserApplication.class.getSimpleName() + " is success!");
    }

}
