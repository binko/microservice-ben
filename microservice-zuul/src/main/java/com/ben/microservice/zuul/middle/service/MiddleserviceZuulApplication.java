package com.ben.microservice.zuul.middle.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiddleserviceZuulApplication {

    public static void main(String[] args) {
        SpringApplication.run(MiddleserviceZuulApplication.class, args);
    }

}
