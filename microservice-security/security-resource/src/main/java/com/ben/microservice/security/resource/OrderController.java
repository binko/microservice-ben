package com.ben.microservice.security.resource;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @GetMapping("/user")
    @PreAuthorize("hasAnyAuthority('admin')") // 拥有p1 权限方可访问url
    public String r1(){
        return "访问资源1";
    }
}
