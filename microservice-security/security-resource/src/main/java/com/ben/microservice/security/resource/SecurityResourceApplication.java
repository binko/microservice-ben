package com.ben.microservice.security.resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.ben")
public class SecurityResourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityResourceApplication.class, args);
    }

}
