package com.ben.microservice.security.authorization;

import com.ben.microservice.security.authorization.properties.SecurityProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@EnableConfigurationProperties(SecurityProperties.class)
public class SecurityAuthorizationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityAuthorizationApplication.class, args);
    }

}
