package com.ben.microservice.security.authorization.controller;

import com.ben.microservice.security.authorization.common.JsonResult;
import com.ben.microservice.security.authorization.common.ResultTool;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.jws.WebResult;

@RestController
public class TestController {

    @GetMapping("/api/hello")
    public JsonResult getHello(){
        return ResultTool.success("hello");
    }

    @GetMapping("/admin/hello")
    public String admin() {
        return "hello admin";
    }

    @GetMapping("/user/hello")
    public String user() {
        return "hello user";
    }

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }
}
