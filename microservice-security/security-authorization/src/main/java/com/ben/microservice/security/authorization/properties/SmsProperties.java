package com.ben.microservice.security.authorization.properties;

import lombok.Data;

@Data
public class SmsProperties {

    private String sendSmsUrl;
}
