package com.ben.microservice.springboot.demo;

import com.ben.microservice.springboot.demo.phoenix.HbaseDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.jdbc.metadata.DataSourcePoolMetadata;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Statement;

@Slf4j
@RestController
@SpringBootApplication
public class MicroserviceDemoApplication {

    @Autowired
    private HbaseDataSource hbaseDataSource;

    @Autowired
    @Qualifier("phoenixJdbcTemplate")
    JdbcTemplate phoenixJdbcTemplate;

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceDemoApplication.class, args);
    }


    @GetMapping("/test")
    public String getHello() throws Exception{

        Connection connection = hbaseDataSource.hbasePhoenixDataSource().getConnection();
        /*Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery("select * from table");
        while (rs.next()){
            System.out.println(rs.getString("id"));
        }*/
        return connection.toString();
    }

    @GetMapping("/test1")
    public String getTest(){

        phoenixJdbcTemplate.execute("CREATE TABLE test ( id BIGINT not null primary key, date Date)");
        return "123";
    }

}
