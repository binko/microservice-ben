package com.ben.microservice.springboot.demo.phoenix;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class PhoenixDataSource {

    @Autowired
    private Environment env;

    @Bean(name = "phoenixJdbcDataSource")
    @Qualifier("phoenixJdbcDataSource")
    public DataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(env.getProperty("hbase.phoenix.datasource.url"));
        dataSource.setDriverClassName(env.getProperty("hbase.phoenix.datasource.driverClassName"));
        dataSource.setUsername(env.getProperty("hbase.phoenix.datasource.username"));//phoenix的用户名默认为空
        dataSource.setPassword(env.getProperty("hbase.phoenix.datasource.password"));//phoenix的密码默认为空
        return dataSource;
    }

    @Bean(name = "phoenixJdbcTemplate")
    public JdbcTemplate phoenixJdbcTemplate(@Qualifier("phoenixJdbcDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

}
