package com.ben.microservice.springboot.demo.phoenix;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class PhoenixTest {
    public static void main(String[] args) throws Exception{
        Class.forName("org.apache.phoenix.queryserver.client.Driver");
        Connection connection = DriverManager.getConnection("jdbc:phoenix:thin:url=http://data.shuiwujia.com:8765;serialization=PROTOBUF");

        PreparedStatement statement = connection.prepareStatement("select * from lable_view");
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()){
            System.out.println(resultSet.getString("ID"));
        }
        statement.close();
        connection.close();

    }
}
