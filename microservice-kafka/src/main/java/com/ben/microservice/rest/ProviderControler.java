package com.ben.microservice.rest;

import com.ben.microservice.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProviderControler {

    @Autowired
    private KafkaService kafkaService;

    @GetMapping("/sendMsg")
    public void sendMsg(String topic, String msg){
        kafkaService.sendMessage(topic,msg);

    }
}
